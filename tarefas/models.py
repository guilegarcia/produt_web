from django.db import models
from usuarios.models import Usuario
from papeis.models import Papel
from projetos.models import Projeto

class Tarefa(models.Model):
    titulo = models.CharField(max_length=150)
    descricao = models.TextField(max_length=500)
    data = models.DateField(blank=True, null=True)
    hora = models.TimeField(null=True, blank=True)
    status = models.IntegerField(default=0)
    fk_papel = models.ForeignKey(Papel, blank=True, null=True)
    fk_projeto = models.ForeignKey(Projeto, blank=True, null=True)
    fk_usuario = models.ForeignKey(Usuario)
    status_tarefa_repetida = models.BooleanField(default=False)
    fk_tarefa_repetida = models.ForeignKey('self', blank=True, null=True)

    class Meta:
        db_table = 'tarefa'

    def __init__(self, *args, **kwargs):
        super(Tarefa, self).__init__(*args, **kwargs)

    def __str__(self):
        return self.titulo
