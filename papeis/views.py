# -*- encoding: utf-8 -*-
from datetime import timedelta
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.messages.views import SuccessMessageMixin
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.views.generic import CreateView, UpdateView

from papeis.forms import FormPapel
from papeis.models import Papel
from tarefas.models import Tarefa


class PapelCreate(SuccessMessageMixin, CreateView):
    model = Papel
    fields = '__all__'
    template_name = 'criar-papel.html'
    success_url = '.'
    success_message = "Cadastro realizado com sucesso!"


class PapelUpdate(SuccessMessageMixin, UpdateView):
    model = Papel
    fields = '__all__'
    template_name = 'criar-papel.html'
    success_url = '.'
    success_message = "Cadastro atualizado com sucesso!"

    def get_success_url(self, **kwargs):
        return reverse('editar_papel', kwargs={'pk': self.kwargs['pk']})


# Papeis.html
@login_required()
def papeis(request):
    lista_papeis = Papel.objects.filter(fk_usuario=request.user)
    request.session['lista_papeis'] = lista_papeis
    return render(request, 'papeis.html', {'papel_active': 'active'})


@login_required()
def excluir_papel(request, id=None):
    papel = get_object_or_404(Papel, fk_usuario=request.user, id=id)
    papel.delete()
    messages.success(request, 'Papel foi excluído com sucesso')
    return redirect(request.META.get('HTTP_REFERER'))


@login_required()
def editar_papel(request):
    if request.method == 'GET':
        papel = get_object_or_404(Papel, id=request.GET['id'], fk_usuario=request.user)
        form = FormPapel(instance=papel)
    else:
        form = FormPapel(request.POST)
        if form.is_valid():
            papel = form.save(commit=False)
            papel.id = form.cleaned_data['id']
            papel.save()
            messages.success(request, 'Papel atualizado com sucesso')
            return redirect('/papeis/')
    return render(request, 'papeis.html',
                  {'form': form, 'abrir_modal_papel': 'in', 'editar_papel': 'true'})


@login_required()
def papel(request, id=None):
    papel = get_object_or_404(Papel, fk_usuario=request.user, id=id)
    lista_tarefas = Tarefa.objects.filter(fk_usuario=request.user, fk_papel=papel)
    return render(request, 'papel.html',
                  {'papel': papel, 'lista_tarefas_papel': lista_tarefas, 'papel_active': 'active'})


