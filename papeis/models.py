from django.db import models
from usuarios.models import Usuario

class Papel(models.Model):
    nome = models.CharField(max_length=150)
    descricao = models.TextField(max_length=500, blank=True, null=True)
    fk_usuario = models.ForeignKey(Usuario)

    class Meta:
        db_table = 'papel'

    def __init__(self, *args, **kwargs):
        super(Papel, self).__init__(*args, **kwargs)

    def __str__(self):
        return self.nome