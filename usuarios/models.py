from django.db import models
from django.contrib.auth.models import AbstractUser

class Usuario(AbstractUser):

    nome = models.CharField(max_length=72)
    login = models.CharField(max_length=30)
    senha = models.CharField(max_length=8)

    class Meta:
        swappable = 'AUTH_USER_MODEL'
        db_table = 'usuario'