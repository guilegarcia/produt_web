from django.db import models
from usuarios.models import Usuario

class Projeto(models.Model):
    nome = models.CharField(max_length=150)
    descricao = models.TextField(max_length=1024, blank=True, null=True)
    Usuarios = models.ManyToManyField(Usuario)

    class Meta:
        db_table = 'projeto'

    def __str__(self):
        return self.nome