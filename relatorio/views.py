from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.db import connection


@login_required()
def relatorios(request):
    return render(request, 'relatorio.html', {
        'relatorio1': relatorio1(),
        'relatorio2': relatorio2(),
        'relatorio3': relatorio3(),
        'relatorio4': relatorio4(),
        'relatorio5': relatorio5(),
        'relatorio_active': 'active'
    })


def relatorio1():
    db = connection.cursor()
    db.execute("""SELECT
                     papel.descricao    AS papel,
                     usuario.first_name AS nome,
                     tarefa.descricao   AS tarefa,
                     projeto.nome       AS projeto
                   FROM tarefa
                     INNER JOIN usuario ON usuario.id = tarefa.fk_usuario_id
                     INNER JOIN projeto ON projeto.id = tarefa.fk_projeto_id
                     INNER JOIN papel   ON papel.id = tarefa.fk_papel_id
                   WHERE tarefa.data BETWEEN '2017-03-01' AND NOW() AND papel.descricao LIKE 'A%' AND projeto.nome LIKE 'A%'
                   GROUP BY tarefa.titulo, tarefa.fk_papel_id, tarefa.fk_projeto_id
                   ORDER BY tarefa.data;
                           """)

    return dictfetchall(db)


def relatorio2():
    db = connection.cursor()
    db.execute("""  SELECT a.titulo, b.reptidos
                            FROM tarefa AS a
                            INNER JOIN (
                                SELECT fk_tarefa_repetida_id  AS id,  COUNT(fk_tarefa_repetida_id) AS reptidos
                                FROM tarefa
                                WHERE status_tarefa_repetida > 0
                                GROUP BY fk_tarefa_repetida_id
                                HAVING reptidos > 2
                            ) AS b ON a.id = b.id
                            WHERE DAYOFWEEK(a.data) NOT IN (1, 4, 7);
                        """)
    return dictfetchall(db)


def relatorio3():
    db = connection.cursor()
    db.execute("""(SELECT DISTINCT (a.descricao)
                    FROM lembrete AS a
                    INNER JOIN (
                        SELECT t.fk_usuario_id
                        FROM tarefa AS t
                        WHERE DAYOFWEEK(t.data) = 4
                    ) AS b
                    )
                    UNION
                    (SELECT DISTINCT (a.descricao)
                    FROM lembrete AS a
                    INNER JOIN (
                        SELECT t.fk_usuario_id
                        FROM tarefa AS t
                        WHERE DAYOFWEEK(t.data) = 5
                    ) AS b)
                    ;
                        """)
    return dictfetchall(db)


def relatorio4():
    db = connection.cursor()
    db.execute("""  SELECT p.nome, b.descricao
                    FROM projeto AS p
                    INNER JOIN (
                        SELECT MAX(t.id)  AS tid, t.fk_projeto_id AS pid,  t.descricao
                        FROM tarefa AS t
                    WHERE t.fk_projeto_id IS NOT NULL
                    GROUP BY t.fk_projeto_id
                    ) AS b ON b.pid = p.id;
                    """)
    return dictfetchall(db)


def relatorio5():
    db = connection.cursor()
    db.execute("""    SELECT
    DAYOFMONTH(a.data) + 1      AS DIA_INICIAL,
    MIN(DAYOFMONTH(c.data)) - 1 AS DIA_FINAL
    FROM tarefa AS a
    LEFT OUTER JOIN tarefa AS b ON DAYOFMONTH(a.data) = DAYOFMONTH(b.data) - 1
    LEFT OUTER JOIN tarefa AS c ON DAYOFMONTH(a.data) < DAYOFMONTH(c.data)
    WHERE DAYOFMONTH(b.data) IS NULL AND DAYOFMONTH(c.data) IS NOT NULL
    AND a.data BETWEEN DATE_FORMAT(NOW(), '%Y-%m-01') AND LAST_DAY(NOW())
    GROUP BY DAYOFMONTH(a.data), DAYOFMONTH(b.data);
                        """)
    return dictfetchall(db)


def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
        ]
