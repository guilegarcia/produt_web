from django.db import models
from usuarios.models import Usuario

class Lembrete(models.Model):
    titulo = models.CharField(max_length=150)
    descricao = models.TextField(max_length=1024, blank=True, null=True)
    data = models.DateField()
    fk_usuario = models.ForeignKey(Usuario)

    class Meta:
        db_table = 'lembrete'

    def __init__(self, *args, **kwargs):
        super(Lembrete, self).__init__(*args, **kwargs)

    def __str__(self):
        return self.titulo